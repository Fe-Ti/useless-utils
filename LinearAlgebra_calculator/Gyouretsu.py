# Gyouretsu - matrix
from __future__ import division

def greatest_divisor(a, b):
	while b!=0:
		t = b
		b = a % b
		a = t
	return a

def least_multiple(a, b):
	return (a*b)/greatest_divisor(a,b)

class Fraction():
	num = 0 # num
	den = 1 # denominator
	def get_num(self):
		return self.num
	def get_den(self):
		return self.den

	def __init__(self, num = 0, den = 1):
		if type(num) == type('string'):
			if (num.find('/')!=-1):
				den = float(num.split('/')[1])
				num = float(num.split('/')[0])
			else:
				num = float(num)
		self.num = num
		self.den = den
		self.reduce_fraction()

	def reduce_fraction(self):
		div = greatest_divisor(self.num, self.den)
		self.num /= div
		self.den /= div

	def parse(self, string = '0/1'):
		string = string.split('/')
		self.num = float(string[0])
		if len(string) == 2:
			self.den = float(string[1])

	def __str__(self):
		if self.num == self.den: 
			self.num = self.num / self.den
			self.den = self.num
			return str(self.num)
		else:
			return str(self.num)+'/'+str(self.den) 

	def __add__(self, other):
		if(self.den != other.den):
			newden = least_multiple(self.den, other.den)
			newnum = self.num * (newden / self.den) + other.num * (newden / other.den)
		else:
			newden = self.den
			newnum = self.num + other.num
		newFraction = Fraction(newnum, newden)
		return newFraction
			
	def __mul__(self, other):
		if type(self) == type(other):
			newFraction = Fraction(self.num * other.num, self.den * other.den)
		else:
			newFraction = Fraction(self.num * other, self.den)
		return newFraction
			
	def __div__(self, other):
		if type(self) == type(other):
			newFraction = Fraction(self.num * other.den, self.den * other.num)
		else:
			newFraction = Fraction(self.num, self.den * other)
		return newFraction
			
	def __truediv__(self, other):
		if type(self) == type(other):
			newFraction = Fraction(self.num * other.den, self.den * other.num)
		else:
			newFraction = Fraction(self.num, self.den * other)
		return newFraction

# ~ a = Fraction(2,4)
# ~ b = Fraction(4,7)
# ~ c = a+b
# ~ print(c)
# ~ c = a*b
# ~ print(c)

class Vector():
	vec = []
	
	def __init__(self, vec = []):
		self.vec = vec[:]
		for i in range(len(vec)):
			self.vec[i] = Fraction(self.vec[i])
		
	def scal_mul(self, other):
		if (len(self.vec) == len(other.vec)):
			result = Fraction()
			for i in range(len(self.vec)):
				result += self.vec[i] * other.vec[i]
		else:
			result = 'Error'
		return result
		
	def __str__(self):
		result = '( '
		for i in self.vec:
			result += str(i) + ' '
		return result + ')'

	def __add__(self, other):
		if (len(self.vec) == len(other.vec)):
			newVec = Vector(self.vec)
			for i in range(len(self.vec)):
				newVec.vec[i] += other.vec[i] 
		else:
			newVec = "Error"
		return newVec
		


class AppLib():
	mode = 1
	curr_matrix = []
	saved_matrix_buffer = []
	objmode = 'fraction'
	
	def __init__(self, mode=1):
		self.mode = mode #int(input('Enter 0 to start indexes from 0 or 1 to start from 1: '))
	
	def cast_float(self):
		for i in range(len(self.curr_matrix)):
			for j in range(len(self.curr_matrix[i])):
				self.curr_matrix[i][j] = float(self.curr_matrix[i][j])
	
	def cast_fraction(self):
		for i in range(len(self.curr_matrix)):
			for j in range(len(self.curr_matrix[i])):
				self.curr_matrix[i][j] = Fraction(self.curr_matrix[i][j])
		
	def input_matrix(self):
		print('Enter the matrix separating elements with whitespaces & new lines (empty line - skip to the next step):')
		self.curr_matrix = []
		line = input()
		row_len = len(line.split())
		while line != '':
			if (row_len == len(line.split())):
				self.curr_matrix.append(line.split())
			else:
				print('Error! Skipping this row (you can try to retype it more carefully).')
			line = input()
	
	def add_xrowA_to_rowB(self, line):
		line[0] = int(line[0]) - self.mode
		if self.objmode == 'fl':
			line[1] = float(line[1])
		else:
			line[1] = Fraction(line[1])
		line[2] = int(line[2]) - self.mode
		if (line[0] < len(self.curr_matrix)) and (line[2] < len(self.curr_matrix)):
			for i in range(len(self.curr_matrix[line[0]])):
				self.curr_matrix[line[2]][i] = self.curr_matrix[line[2]][i] + self.curr_matrix[line[0]][i] * line[1]
	
	def divide_row_by_x(self, line):
		line[0] = int(line[0]) - self.mode
		if self.objmode == 'fl':
			line[1] = float(line[1])
		else:
			line[1] = Fraction(line[1])
		if (line[0] < len(self.curr_matrix)):
			for i in range(len(self.curr_matrix[line[0]])):
				self.curr_matrix[line[0]][i] = self.curr_matrix[line[0]][i] / line[1]
	
	def gaussmethod_c(self):
		self.objmode = input('Enter "fl" to use float instead of fractions (or press "Return"): ')
		self.input_matrix()
		if self.objmode == 'fl':
			self.cast_float()
		else:
			self.cast_fraction()
		self.gaussmprint()
		line = input('Enter <source row> <multiplier> <destination>\n'
					'or <source row> <divider>:\n')
		while line!='' and line!='save' and line!= 'exit':
			line = line.split()
			if (len(line) == 3):
				self.add_xrowA_to_rowB(line)
			elif (len(line) == 2):
				self.divide_row_by_x(line)
			else:
				print('Error!')
			print('Matrix state:')
			self.gaussmprint()
			line = input('Enter <source line> <multiplier> <dest. line>:\n'
					'or <source row> <divider>:\n')
		if line == 'exit':
			exit
		elif line == 'save':
			self.saved_matrix_buffer = self.curr_matrix

	def gaussmprint(self):
		maxlen = 0
		string_matrix = []
		for i in range(len(self.curr_matrix)):
			string_matrix.append(self.curr_matrix[i][:])
			for j in range(len(self.curr_matrix[i])):
				string_matrix[i][j] = str(self.curr_matrix[i][j])
				if maxlen < len(string_matrix[i][j]):
					maxlen = len(string_matrix[i][j])
		for i in string_matrix:
			for j in range(len(i)-1):
				print(f"{i[j]:{maxlen}}",end='|')
			print('|',i[-1], sep='')

a = AppLib(1)
line = input('Enter "gm" for gauss method, "vsc" - for scalar multiplication\nof two vectors or "exit" to quit the program: ')
while line != 'exit':
	if line == 'gm':
		a.gaussmethod_c()
	elif line == 'vsc':
		m = input('Enter the first vector (separate values via a whitespace):\n')
		m = m.split()
		a = Vector(m)
		m = ''
		m = input('Enter the second vector:\n')
		m = m.split()
		b = Vector(m)
		print(a.scal_mul(b))
	line = input('Enter "gm" for gauss method, "vsc" - for scalar multiplication\nof two vectors or "exit" to quit the program: ')
