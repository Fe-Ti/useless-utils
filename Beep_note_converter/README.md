#### Converts stuff from strings formated like "C41234 A12344 ..." into the strings which can be used via beep command.
Format is `LODDD...`:

- L - letter of the note
- O - its octave
- D - its duration

## Example:
g41020 c41020 D4170 f4170 g4680 c4680 D4170 f4170 d4340 g3340 A3170 c4170 d4340 g3340 A3170 c4170 d4340 g3340 A3170 c4170 d4340 g3340 A3170 c4170 f41020 A31020 d4170 D4170 f4680 A3680 D4170 d4170 c41020 g4340 c4340 D4170 f4170 g4340 c4340 D4170 f4170 g4340 c4340 D4170 f4170 <--here is a whitespace after the last note
## Result:
beep   -f 392 -l 1020 -n -f 262 -l 1020 -n -f 311 -l 170 -n -f 349 -l 170 -n -f 392 -l 680 -n -f 262 -l 680 -n -f 311 -l 170 -n -f 349 -l 170 -n -f 294 -l 340 -n -f 196 -l 340 -n -f 233 -l 170 -n -f 262 -l 170 -n -f 294 -l 340 -n -f 196 -l 340 -n -f 233 -l 170 -n -f 262 -l 170 -n -f 294 -l 340 -n -f 196 -l 340 -n -f 233 -l 170 -n -f 262 -l 170 -n -f 294 -l 340 -n -f 196 -l 340 -n -f 233 -l 170 -n -f 262 -l 170 -n -f 349 -l 1020 -n -f 233 -l 1020 -n -f 294 -l 170 -n -f 311 -l 170 -n -f 349 -l 680 -n -f 233 -l 680 -n -f 311 -l 170 -n -f 294 -l 170 -n -f 262 -l 1020 -n -f 392 -l 340 -n -f 262 -l 340 -n -f 311 -l 170 -n -f 349 -l 170 -n -f 392 -l 340 -n -f 262 -l 340 -n -f 311 -l 170 -n -f 349 -l 170 -n -f 392 -l 340 -n -f 262 -l 340 -n -f 311 -l 170 -n -f 349 -l 170