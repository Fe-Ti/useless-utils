// Copyright 2021 Fe-Ti
// sBF intrpr
// TODO: Refactoring is needed

#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <stack>
#include <string>
#include <vector>

const size_t OBS = 1024;
const size_t DS = 4096;

class SBFI;
int
nop(SBFI& intrpr);
int
next_cell(SBFI& intrpr);
int
prev_cell(SBFI& intrpr);
int
increment_cell(SBFI& intrpr);
int
decrement_cell(SBFI& intrpr);
int
read_from_stdin(SBFI& intrpr);
int
write_to_stdout(SBFI& intrpr);
int
enter_loop(SBFI& intrpr);
int
exit_loop(SBFI& intrpr);
int
stop(SBFI& intrpr);
//#include "opcode.h"

class SBFI
{
  public:
    std::vector<uint8_t> _opcode_buf;
    std::vector<uint8_t> _data; //
    std::stack<size_t> _loop_begins;
    std::stack<size_t> _loop_ends;
    size_t opcode_ptr;
    size_t data_ptr;
    int ret_code = 0; // return value of operation

    const std::map<std::string, uint8_t> s2opcodemap = {
        { "NOP", 0 },  { "NC", 1 },   { "PC", 2 },    { "INC", 3 },
        { "DEC", 4 },  { "READ", 5 }, { "WRITE", 6 }, { "LOOP", 7 },
        { "LEND", 8 }, { "STOP", 9 }, { "ADD", 10 },  { "SUB", 11 },
        { "CMP", 12 }, { "JMP", 13 }, { "MUL", 14 },  { "DIV", 15 }
    };

    const std::map<uint8_t, std::function<int(SBFI& intrpr)>> opcodemap = {
        { 0, nop },
        { 1, next_cell },
        { 2, prev_cell },
        { 3, increment_cell },
        { 4, decrement_cell },
        { 5, read_from_stdin },
        { 6, write_to_stdout },
        { 7, enter_loop },
        { 8, exit_loop },
        { 9, stop },
        { 10, nop },
        { 11, nop },
        { 12, nop },
        { 13, nop },
        { 14, nop },
        { 15, nop },
    };

    SBFI(size_t opcode_buf_size = OBS, size_t data_size = DS)
    {
        _data.resize(data_size);
        opcode_ptr = 0;
        data_ptr = 0;
    }

    uint8_t translate(const std::string& sopcode)
    {
        return s2opcodemap.at(sopcode);
    }

    void load_excutable(const std::string& name = "main.sbf")
    {
        std::ifstream ex(name);
        size_t counter = 0;
        std::string sopcode;
        ex >> sopcode;
        while(!ex.eof()) {
            // Debug:
            // std::cout << "Looking at opcode: " << sopcode << std::endl;
            try {
                _opcode_buf.push_back(translate(sopcode));
            } catch(const std::out_of_range& critical_error) {
                std::cout << "Unknown opcode: " << sopcode << std::endl;
                std::exit(2);
            }
            // Here should be some sort of variables
            // But I'm writing some sort of brainfuck intrpr
            // So not this time
            sopcode.clear();
            ex >> sopcode;
        }
    }

    int run()
    {
        uint8_t curr_opcode = 0;
        while(ret_code != -1 && opcode_ptr < _opcode_buf.size()) {
            curr_opcode = _opcode_buf[opcode_ptr];
            ret_code = opcodemap.at(curr_opcode)(*this);
            ++opcode_ptr;
        }
        return ret_code;
    }
};

int
nop(SBFI& intrpr)
{
    return 0;
}
int
next_cell(SBFI& intrpr)
{
    ++intrpr.data_ptr;
    return 0;
}
int
prev_cell(SBFI& intrpr)
{
    --intrpr.data_ptr;
    return 0;
}
int
increment_cell(SBFI& intrpr)
{
    ++intrpr._data[intrpr.data_ptr];
    return 0;
}
int
decrement_cell(SBFI& intrpr)
{
    --intrpr._data[intrpr.data_ptr];
    return 0;
}
int
read_from_stdin(SBFI& intrpr)
{
    std::cin >> intrpr._data[intrpr.data_ptr];
    return 0;
}
int
write_to_stdout(SBFI& intrpr)
{
    std::cout << intrpr._data[intrpr.data_ptr];
    return 0;
}
int
enter_loop(SBFI& intrpr)
{
    if(intrpr._data[intrpr.data_ptr])
        intrpr._loop_begins.push(intrpr.opcode_ptr);
    else {
        if(!intrpr._loop_ends.empty()) {
            intrpr.opcode_ptr = intrpr._loop_ends.top();
            //_loop_ends.pop();
        } else {
            while(intrpr._opcode_buf[intrpr.opcode_ptr] != 8) {
                ++intrpr.opcode_ptr;
            }
        }
    }
    return 0;
}
int
exit_loop(SBFI& intrpr)
{
    if(intrpr._data[intrpr.data_ptr]) {
        intrpr._loop_ends.push(intrpr.opcode_ptr);
        if(!intrpr._loop_begins.empty()) {
            intrpr.opcode_ptr = intrpr._loop_begins.top();
        } else {
            return -2;
        }
    }
    return 0;
}
int
stop(SBFI& intrpr)
{
    return -1;
}

// main

#include <chrono>
#include <ctime>

int
main()
{
    SBFI test_run;
    auto start = std::chrono::steady_clock::now();
    test_run.load_excutable();
    auto load_end = std::chrono::steady_clock::now();
    test_run.run();
    auto run_end = std::chrono::steady_clock::now();
    auto start_c = std::chrono::steady_clock::now();
    std::cout << "HELLO WORLD" << std::endl;
    auto end_c = std::chrono::steady_clock::now();
    auto load_time =
      std::chrono::duration_cast<std::chrono::microseconds>(load_end - start)
        .count();
    auto run_time =
      std::chrono::duration_cast<std::chrono::microseconds>(run_end - load_end)
        .count();
    auto total_time =
      std::chrono::duration_cast<std::chrono::microseconds>(run_end - start)
        .count();
    auto c_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_c - start_c)
        .count();
    std::cout << "SBFI" << std::endl;
    std::cout << "    Load time " << load_time << std::endl;
    std::cout << "    Run time " << run_time << std::endl;
    std::cout << "    Total time " << total_time << std::endl;
    std::cout << "C++ time " << c_time << std::endl;
    return 0;
}
