import socket
import sys
import time

ENCODING="utf-8"

HOST, PORT = "localhost", 9999
data = open("".join(sys.argv[1:]), "rb")
data.seek(0, 2)
data_size = data.tell()
print(data_size)
data.seek(0)

sentsize = 0
# Create a socket (SOCK_STREAM means a TCP socket)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    # send size of file
    sock.sendall(bytes(str(data_size), ENCODING))

    # Receive data from the server and shut down
    received = sock.recv(1024).strip()
    while received != b'OK':
        print('Waiting...')
        time.sleep(3)
        sock.sendall(bytes("Can print?\n", ENCODING))
        received = sock.recv(1024).strip()
        print(received)
        
    sentsize = sock.sendfile(data)
    received = sock.recv(1024).strip()



data.close()
print("Sent:     {} bytes".format(sentsize))
print("Received: {}".format(received))
