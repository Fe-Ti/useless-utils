import socketserver
import subprocess
ENCODING="utf-8"

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        print(self.data)
        self.request.sendall(bytes("OK\n", ENCODING))
        buffsize = int(self.data) - 1024
        print(buffsize)
        self.data = self.request.recv(1024).strip()
        while buffsize > 0:
            if buffsize >1024:
                self.data += self.request.recv(1024)
                buffsize -= 1024
            else:
                self.data += self.request.recv(buffsize)
                buffsize = 0


        print("{} wrote:".format(self.client_address[0]))
        f = open("docs2print", "wb")
        f.write(self.data)
        f.close()
        # ~ proc = subprocess.Popen()
        # ~ try:
            # ~ outs, errs = proc.communicate(timeout=15)
        # ~ except TimeoutExpired:
            # ~ proc.kill()
            # ~ outs, errs = proc.communicate()
        # ~ print(self.data)
        # just send back the same data, but upper-cased
        self.request.sendall(bytes("Printed!\n", ENCODING))#self.data.upper())

if __name__ == "__main__":
    HOST, PORT = "localhost", 9999

    # Create the server, binding to localhost on port 9999
    with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
